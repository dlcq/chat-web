import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import Header from "@/components/Header.vue";

describe("Header.vue", () => {
  it("renders props.msg when passed", () => {
    const userName = "dlcq";
    const wrapper = shallowMount(Header, {
      propsData: { userName }
    });
    expect(wrapper.text()).to.include(userName);
  });
});
