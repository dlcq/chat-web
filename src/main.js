import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import api from "./api";

Vue.config.productionTip = false;
Vue.prototype.$api = api;

console.log(process.env)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
